---
layout: page
lang: pt
---

# Ladainhas

{% for ladainha in site.ladainhas %}

[{{ ladainha.title}}]( {{ ladainha.url }} )

{% endfor%}

# Corridos

{% for corrido in site.corridos %}

[{{ corrido.title}}]( {{ corrido.url }} )

{% endfor%}
