---
layout: page
title: Grabaciones
permalink: /grabaciones/
---

## Grabación Mariana, Fede, Victor y Martín - 26/07/2021

<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/1294783876&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/martin-ceresa" title="Martin Ceresa" target="_blank" style="color: #cccccc; text-decoration: none;">Martin Ceresa</a> · <a href="https://soundcloud.com/martin-ceresa/sets/marianita-fede-vic-martin" title="Marianita, Fede, Vic, Martín" target="_blank" style="color: #cccccc; text-decoration: none;">Marianita, Fede, Vic, Martín</a></div>
