#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json


import os  # I need access to some OS functionalities
import sys  # Command line arguments

assert len(sys.argv) > 2
dest = sys.argv[1]
cantigas = sys.argv[2]
# dirs = sys.argv[2:]


def biFrase(fstFrase, sndFrase):
    return fstFrase + "\n  " + sndFrase

# Frase = Bifrase | Frase | Simples | Quverso
def toTexFrase(json_frase):
    if "bifrase" in json_frase:
        assert len(json_frase["bifrase"]) == 2
        return biFrase(json_frase["bifrase"][0], json_frase["bifrase"][1])
    if "frase" in json_frase:
        return json_frase["frase"]
    if "simples" in json_frase:
        res = ""
        res += json_frase["simples"][0]
        for simp in json_frase["simples"][1:]:
            res += "\n" + simp
        return res
    # QuVersos
    if "quverso" in json_frase:
        assert len(json_frase["quverso"]) == 4
        res = biFrase(json_frase["quverso"][0], json_frase["quverso"][1])
        res += "\n"
        res += biFrase(json_frase["quverso"][2], json_frase["quverso"][3])
        return res
    return "NINGUNA FRASE!"


# Json_forma : Frase | [Frase]
def toTexForma(json_forma):
    res = ""
    # Frases
    if "frases" in json_forma:
        res += toTexFrase(json_forma["frases"][0])
        for fra in json_forma["frases"][1:]:
            res += "\n"
            res += toTexFrase(fra)
        return res
    # Plain frase
    return toTexFrase(json_forma)


# Songs: json \to tex
def toTex(json_song):
    ### Header
    resultado = "---\n"
    resultado += "title: " + json_song["title"] + "\n"
    if "author" in json_song:
        resultado += "author: " + json_song["author"] + "\n"
    else:
        resultado += "author: Dominio Público\n"
    if "chorus" in json_song:
        chorus = toTexForma(json_song["chorus"])
    else:
        chorus = "No Coro"
    ##
    resultado += "layout: page\n"
    resultado += "lang: pt\n"
    resultado += "---\n"
    ##3 Content
    ## Youtube media
    if "media" in json_song:
        if "youtube" in json_song["media"]:
            resultado += (
                '<iframe width="560" height="315" src="https://www.youtube.com/embed/'
                + json_song["media"]["youtube"]
                + '" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
            )
    assert "versos" in json_song
    for verso in json_song["versos"]:
        ## First we check if it is a chorus.
        resultado += "```\n"
        if "coro" in verso:
            resultado += "*Coro*\n"
            if not verso["coro"] == None:
                chorus = toTexForma(verso["coro"])
            resultado += "NO CORO" if chorus is None else chorus
        # Otherwise is some sort of verse.
        else:
            res = toTexForma(verso)
            resultado += "ERROR" if res is None else res
        resultado += "\n```\n"
    ##
    ## Cantiga
    return resultado


print("Cannot open cantigas?")
for dir in os.listdir(cantigas):
    # texfile = open(dest + "/" + dir + ".markdown", "w")
    print("Guarando en:" + dest + "/_" + dir + "\n")
    for cantiga in os.listdir(cantigas + "/" + dir):
        cantiganm = "/".join([cantigas, dir, cantiga])
        dstfile = os.path.splitext("/".join([dest, "_" + dir, cantiga]))[0]
        cantigafile = open(dstfile + ".markdown", "w")
        if cantiga.endswith(".json"):
            print("Translating: " + cantiga + "\n")
            cantigaJsonFile = open(cantiganm, "r")
            jsontiga = json.load(cantigaJsonFile)
            cantigaJsonFile.close()
            cantigafile.write(toTex(jsontiga))
        cantigafile.close()
