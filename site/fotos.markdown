---
layout: page
title: Fotos
permalink: /fotos/
---

Álbumes de fotos de Capoeira.

- [Entrenamiento y Roda - 23/07/2021](https://photos.app.goo.gl/s9GPqQBPDwv27KQv9){:target="\_blank"} + [Casa de Al Lado](https://photos.app.goo.gl/pGkKKwBfpjgxNv5C8){:target="\_blank"}
- [Roda Club Estudiantil - 16/07/2021](https://photos.app.goo.gl/DTVRXubjnAg2GWTE7){:target="\_blank"}
- [Festejo Cumple Anto - 11/07/2021](https://photos.app.goo.gl/YgSG6NXG7W9JQbkg8){:target="\_blank"}
- [Entrenamiento - 9/07/2021](https://photos.app.goo.gl/hfJKkmn5JzXiyz2W7){:target="\_blank"}
