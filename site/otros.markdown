---
layout: page
title: Otros Sitios
permalink: /otros/
---

Otros sitios donde se puedan encontrar más conciones son:

- [Capoeira Music](http://www.capoeira-music.net/): letras acompañadas de vídeos.
- [Cantigas de Capoeira](https://capoeiradb.wordpress.com/): más letras y vídeos :D.
- [Capoeira Lyrics](https://capoeiralyrics.info/) : letras sueltas
- [Papoeira](https://papoeira.com/) : Material de lectura, y música, en diferentes idiomas.
