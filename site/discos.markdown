---
layout: page
title: Discos
permalink: /discos/
---

Voy a ir listando los discos de capoeira que me van pareciendo interesantes:

- [Caminhada vol.1 - Mestre Pernalonga](https://www.youtube.com/watch?v=jUc2LfLT5KU)
- [Raiz Do Feitiço - Mestre Pedrinho de Caxias](https://www.youtube.com/watch?v=iU7qmqq_3ww)
- [Capoeira de Besouro - Paulo César Pinheiro](https://www.youtube.com/watch?v=fra75KTApwc)
- [Original ao vivo - Mestre Ananias](https://www.youtube.com/watch?v=62OPIjGPdUY)
- [Movimento Novo](https://www.youtube.com/watch?v=HiGiM-CZW94)
- [Feira de Cantigas](https://www.youtube.com/watch?v=hh_XIcdTSJU)
- [Capoeira Marrom](https://www.youtube.com/watch?v=tGnuVnjxOow)
- [Cantigas de roda de Capoeira com Mestre Ramos](https://www.youtube.com/watch?v=fW-B8rI9iv4)
- [Musica de Capoeira - Mestre Liminha](https://www.youtube.com/watch?v=1Ao7p5uNBgs)
