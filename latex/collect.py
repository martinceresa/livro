#!/usr/bin/env python3
#

import os  # I need access to some OS functionalities
import sys  # Command line arguments

# JSon Objects
import json

assert len(sys.argv) > 2
dest = sys.argv[1]
cantigas = sys.argv[2]
# dirs = sys.argv[2:]


def biFrase(fstFrase, sndFrase):
    return "\\bifrase%\n{" + fstFrase + "}\n{" + sndFrase + "}"


# Frase = Bifrase | Frase | Simples | Quverso
def toTexFrase(json_frase):
    if "bifrase" in json_frase:
        return biFrase(json_frase["bifrase"][0], json_frase["bifrase"][1]) + "\n"
    if "frase" in json_frase:
        return json_frase["frase"] + "\n"
    if "simples" in json_frase:
        res = ""
        for simp in json_frase["simples"]:
            res += "{" + simp + "}\n\n"
        return res
    # QuVersos
    if "quverso" in json_frase:
        assert len(json_frase["quverso"]) == 4
        res = biFrase(json_frase["quverso"][0], json_frase["quverso"][1])
        res += "\n\n"
        res += biFrase(json_frase["quverso"][2], json_frase["quverso"][3])
        return res
    return "NINGUNA FRASE!"


# Json_forma : Frase | [Frase]
def toTexForma(json_forma):
    res = ""
    # Frases
    if "frases" in json_forma:
        for fra in json_forma["frases"]:
            res += "\n"
            res += toTexFrase(fra)
        return res
    # Plain frase
    return toTexFrase(json_forma)


# Songs: json \to tex
def toTex(json_song):
    resultado = "\\begin{cantiga}{"
    resultado += json_song["title"]
    resultado += "}"
    if "author" in json_song:
        resultado += "{" + json_song["author"] + "}\n"
    else:
        resultado += "{Dominio Público}\n"
    if "chorus" in json_song:
        chorus = toTexForma(json_song["chorus"])
    else:
        chorus = "No Coro"
    ##
    assert "versos" in json_song
    for verso in json_song["versos"]:
        ## First we check if it is a chorus.
        if "coro" in verso:
            resultado += "\\begin{refrao}\n"
            if not verso["coro"] == None:
                chorus = toTexForma(verso["coro"])
            resultado += "NO CORO" if chorus is None else chorus
            resultado += "\n"
            resultado += "\\end{refrao}\n"
        # Otherwise is some sort of verse.
        else:
            resultado += "\\begin{versiculo}\n"
            res = toTexForma(verso)
            resultado += str("ERROR" if res is None else res) + "\n"
            resultado += "\\end{versiculo}\n"
    ##
    ## Cantiga
    resultado += "\\end{cantiga}\n"
    return resultado


for dir in os.listdir(cantigas):
    texfile = open(dest + "/" + dir + ".tex", "w")
    print("Guarando en:" + dest + "/" + dir + ".tex" + "\n")
    for cantiga in os.listdir(cantigas + "/" + dir):
        cantiganm = "/".join([cantigas, dir, cantiga])
        # if cantiga.endswith(".tex"):
        #     print("Adding: " + cantiga + "\n")
        #     if not cantiga == "":
        #         texfile.write("\\input{" + cantiganm + "}\n")
        if cantiga.endswith(".json"):
            print("Translating: " + cantiga + "\n")
            cantigaJsonFile = open(cantiganm, "r")
            jsontiga = json.load(cantigaJsonFile)
            cantigaJsonFile.close()
            # print('Json :' + jsontiga + '\n')
            texfile.write(toTex(jsontiga))
    texfile.close()
